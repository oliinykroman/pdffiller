var plugins = [
    // jquery
    './node_modules/jquery/dist/jquery.js',

    // air-datepicker
    './node_modules/air-datepicker/dist/js/datepicker.js',
    './node_modules/air-datepicker/dist/js/i18n/datepicker.en.js'
];

module.exports = {
    plugins: plugins
}