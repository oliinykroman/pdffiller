$(document).ready(function(){
	// header dropdown 
	$(document).on("click", ".dropdownToggleJs", function(e){
		e.preventDefault();
		$(this).toggleClass('active');
		$(this).closest('.dropdown').find('.dropdown-menu').toggleClass('show')
	});
	$(document).on('click', function (e) {
	    var dropdown = $(".dropdown");
	    if (dropdown.has(e.target).length === 0){
	    	$(this).find('.dropdown-menu').removeClass('show');
	    	$('.dropdownToggleJs').removeClass('active');
	    }
	});

	// close info section
	$(document).on("click", ".closeInfoJs", function(e){
		$(this).closest('.info-holder').slideUp(400);
	});

	// datepicker
	$('.datepicker-filter').datepicker({language: 'en'}).data('datepicker').selectDate(new Date(2020, 8));


	// table
	var html = '';
	$.ajax({
		url: 'assets/js/customer_table.json',
		type: 'GET',
		dataType: 'json',
	}).done(function(json_data) {
		console.log(json_data.table);
		$.each(json_data.table,function (row_id, tds) {
	        switch(row_id){
	            case 0:
	                html    += "<thead>";
	                break;
	            default:
	                break;
	        }
	        html += "<tr>";
	        $.each(tds,function(item_id,td_text){
	            switch(row_id){
	                case 0:
	                    html    += "<th";
	                    break;
	                default:
	                    html    += "<td";
	                    break;
	            }
	            html    += " id='td-"+row_id+'-'+item_id+"'>"+td_text+"</";
	            switch(row_id){
	                case 0:
	                    html    += "th>";
	                    break;
	                default:
	                    html    += "td>";
	                    break;
	            }
	        })
	        html    += "</tr>";
	        switch(row_id){
	            case 0:
	                html    += "</thead>";
	                break;
	            default:
	                break;
	        }
	    });
	    // html    += "</table>";
	    $('table.customer-table').prepend(html);
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});

});